# Lists of images to build. Each image needs to have a corresponding section
# below.
[runtime]
bases = base-debian, base-debian10, base-debian11, base-debiansid, base-debiansid-llvm
ci = ci-python3.6, ci-python3.7, ci-python3.8
toolchains =
    gcc,
    gcc-8,
    gcc-9,
    gcc-10,
    clang,
    clang-10,
    clang-11,
    clang-nightly,
    llvm,
    llvm-10,
    llvm-11,
    llvm-nightly,

# Image specifications.
#
# The following fields are mandatory:
#
# - kind: kind of container. The kind must be implemented in
#   support/docker/configure
#
# - base: base image. for images with kind = "base", can be any image. for the
#   others, needs to be an existing image defined in this file.
#
# - hosts: the hosts where this image is to be built. Most images should build
#   on amd64 and arm64 (the default)
#
# - rebuild: when to rebuild this image. currently only monthly is supported.
#
# Optional fields:
#
# - target_bases: alternative base images for some targets. Format:
#   comma-separated list; each item must be <targetarch>:<baseimage>.
#   Example:

#   target_bases = riscv:gcc-10, arc:gcc-9
#
# - target_kinds = alternative image kind for some targets. Format:
#   comma-separated list; each item must be <targetarch>:<kind>
#   Example:

#   target_kinds = arc:arc, xxx:yyy
#
# - target_hosts = list of hosts where to build image for this specific target.
#   Format: comma separated list; each item is a list separated by +.
#   Example:
#   target_hosts = mips:amd64, arc:amd64, arm64:amd64+arm64

[base-debian]
kind    = base
base    = debian:stable-slim
hosts   = amd64, arm64
rebuild = monthly

[base-debian10]
kind    = base
base    = debian:10-slim
hosts   = amd64, arm64
rebuild = monthly

[base-debian11]
kind    = base
base    = debian:bullseye-slim
hosts   = amd64, arm64
rebuild = monthly

[base-debiansid]
kind    = base
base    = debian:sid-slim
hosts   = amd64, arm64
rebuild = monthly

[base-debiansid-llvm]
kind    = base-llvm
base    = base-debiansid
hosts   = amd64, arm64
rebuild = monthly

[ci-python3.6]
kind    = ci
base    = ubuntu:18.04
rebuild = monthly
hosts   = amd64, arm64

[ci-python3.7]
kind    = ci
base    = debian:10-slim
rebuild = monthly
hosts   = amd64, arm64

[ci-python3.8]
kind    = ci
base    = ubuntu:20.04
rebuild = monthly
hosts   = amd64, arm64

[gcc]
kind    = gcc-build
base    = base-debian
hosts   = amd64, arm64
rebuild = monthly
targets = x86_64, arm64, i386, arm, mips, riscv, arc, parisc, powerpc, s390, sh, sparc
target_bases = riscv:gcc-10, arc:gcc-9
target_kinds = arc:arc
target_hosts = mips:amd64, arc:amd64, parisc:amd64, powerpc:amd64, s390:amd64, sh:amd64, sparc:amd64
packages = gcc, g++

[gcc-8]
kind = gcc-build
base = base-debian10
hosts = amd64, arm64
rebuild = monthly
targets = x86_64, arm64, i386, arm, mips, riscv, arc, parisc, powerpc, s390, sh, sparc
target_kinds = arc:arc
target_hosts = mips:amd64, riscv:amd64, arc:amd64, parisc:amd64, powerpc:amd64, s390:amd64, sh:amd64, sparc:amd64
packages = gcc-8, g++-8

[gcc-9]
kind = gcc-build
base = base-debian11
hosts = amd64, arm64
targets = x86_64, arm64, i386, arm, mips, riscv, arc, parisc, powerpc, s390, sh, sparc
rebuild = monthly
target_bases = mips:base-debiansid
target_kinds = arc:arc
target_hosts = arc:amd64, riscv:amd64, parisc:amd64, powerpc:amd64, sh:amd64, sparc:amd64
packages = gcc-9, g++-9

[gcc-10]
kind = gcc-build
base = base-debian11
hosts = amd64, arm64
targets = x86_64, arm64, i386, arm, mips, riscv, parisc, powerpc, s390, sh, sparc
target_bases = mips:base-debiansid
target_hosts = parisc:amd64, powerpc:amd64, sh:amd64, sparc:amd64
rebuild = monthly
packages = gcc-10, g++-10

[clang]
kind = clang-build
base = base-debiansid
hosts = amd64, arm64
targets = x86_64, arm64, i386, arm, mips, riscv, powerpc, s390, sparc
rebuild = monthly
packages = clang-10, llvm-10, lld-10
target_hosts = mips:amd64, sparc:amd64

[clang-10]
kind = clang-build
base = base-debiansid
hosts = amd64, arm64
targets = x86_64, arm64, i386, arm, mips, riscv, powerpc, s390, sparc
rebuild = monthly
packages = clang-10, llvm-10, lld-10
target_hosts = mips:amd64, sparc:amd64

[clang-11]
kind = clang-build
base = base-debiansid
hosts = amd64, arm64
targets = x86_64, arm64, i386, arm, mips, riscv, powerpc, s390, sparc
rebuild = monthly
packages = clang-11, llvm-11, lld-11
target_hosts = mips:amd64, sparc:amd64

[clang-nightly]
kind = clang-build
base = base-debiansid-llvm
# LLVM nightly snapshots are x86-only:
hosts = amd64
targets = x86_64, arm64, i386, arm, mips, riscv, powerpc, s390, sparc
rebuild = daily
packages = clang, llvm, lld
